/*************************************************************************/
/*  tiled2pmd.cpp                                                        */
/*************************************************************************/
/*                       This file is part of:                           */
/*                            Polygonauts                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* description:                                                          */
/* tiled2pmd is a tool for generating Polygonauts' .pmd files.           */
/*                                                                       */
/*************************************************************************/
/* Copyright (c) 2018-2020 Sinz Group.                                   */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
#include "tinyxml2.h"

#include <iostream>
#include <fstream>
#include <vector>

void process_file(const char* filename)
{
    tinyxml2::XMLDocument tmxfile;
    tmxfile.LoadFile(filename);

    if (tmxfile.ErrorID() != 0) {
        std::cerr << "Error: Reading " << filename << ": " << tmxfile.ErrorID()
            << std::endl;
        return;
    }

    auto tileset = tmxfile.FirstChildElement("map")
        ->FirstChildElement("tileset");

    if (tileset == nullptr) {
        std::cerr << "Error: Failed to find \'tileset\' element\n";
        return;
    }

    int tilewidth = 0, tileheight = 0, tilecount = 0, columns = 0;
    const char* image = "";

    tileset->QueryAttribute("tilewidth", &tilewidth);
    tileset->QueryAttribute("tileheight", &tileheight);
    tileset->QueryAttribute("tilecount", &tilecount);
    tileset->QueryAttribute("columns", &columns);

    tileset->FirstChildElement("image")
        ->QueryStringAttribute("source", &image);

    std::string image_str(image);

    auto last_slash = image_str.rfind("../") + 3;
    image_str = image_str.substr(last_slash, image_str.length() - last_slash);

    auto layer = tmxfile.FirstChildElement("map")
        ->FirstChildElement("layer");

    auto obj_group = tmxfile.FirstChildElement("map")
        ->FirstChildElement("objectgroup");

    if (layer == nullptr) {
        std::cerr << "Error: Failed to find any tilemap layer\n";
        return;
    }

    if (obj_group == nullptr) {
        std::cerr << "Error: Failed to find any object groups\n";
        return;
    }

    tinyxml2::XMLElement* bg_layer = nullptr;
    tinyxml2::XMLElement* main_layer = nullptr;
    tinyxml2::XMLElement* fg_layer = nullptr;
    tinyxml2::XMLElement* physic = nullptr;
    tinyxml2::XMLElement* entities = nullptr;

    while (layer != nullptr) {

        const char* layer_name = "";

        layer->QueryStringAttribute("name", &layer_name);

        if (std::strcmp(layer_name, "tiles-bg") == 0) {
            bg_layer = layer;
        }
        if (std::strcmp(layer_name, "tiles-main") == 0) {
            main_layer = layer;
        }
        if (std::strcmp(layer_name, "tiles-fg") == 0) {
            fg_layer = layer;
        }

        layer = layer->NextSiblingElement("layer");
    }

    if (bg_layer == nullptr) {
        std::cout << "Info: No background layers present in " << filename << '\n';
    }
    if (fg_layer == nullptr) {
        std::cout << "Info: No foreground layers present in " << filename << '\n';
    }
    if (main_layer == nullptr) {
        std::cerr << "Error: No main layer present in " << filename << '\n';
        std::cerr << "No output file will be generated...\n";
        return;
    }

    while (obj_group != nullptr) {
        const char* group_name = "";

        obj_group->QueryStringAttribute("name", &group_name);

        if (std::strcmp(group_name, "physic") == 0) {
            physic = obj_group;
        }
        if (std::strcmp(group_name, "entities") == 0) {
            entities = obj_group;
        }

        obj_group = obj_group->NextSiblingElement("objectgroup");
    }

    std::ofstream outfile;
    auto outfilename = std::string(filename);

    outfilename = outfilename.substr(0, outfilename.find(".tmx"));
    outfile.open(outfilename);

    if (!outfile.is_open()) {
        std::cerr << "Failed to create file '" << outfilename << '\'';
        return;
    }

    outfile << "[tileset]" << '\n';
    outfile << "count = " << tilecount << '\n';
    outfile << "columns = " << columns << '\n';
    outfile << "width = " << tilewidth << '\n';
    outfile << "height = " << tileheight << '\n';
    outfile << "image = \"" << image_str << "\"\n\n";

    int tilemap_width = 0, tilemap_height = 0;

    bg_layer->QueryAttribute("width", &tilemap_width);
    bg_layer->QueryAttribute("height", &tilemap_height);
    auto data = bg_layer->FirstChildElement("data")->GetText();

    outfile << "[tilemap.background]" << '\n';
    outfile << "width = " << tilemap_width << '\n';
    outfile << "height = " << tilemap_height << '\n';
    outfile << "data = [" << data << "]\n\n";

    main_layer->QueryAttribute("width", &tilemap_width);
    main_layer->QueryAttribute("height", &tilemap_height);
    data = main_layer->FirstChildElement("data")->GetText();

    outfile << "[tilemap.main]" << '\n';
    outfile << "width = " << tilemap_width << '\n';
    outfile << "height = " << tilemap_height << '\n';
    outfile << "data = [" << data << "]\n\n";

    fg_layer->QueryAttribute("width", &tilemap_width);
    fg_layer->QueryAttribute("height", &tilemap_height);
    data = fg_layer->FirstChildElement("data")->GetText();

    outfile << "[tilemap.foreground]" << '\n';
    outfile << "width = " << tilemap_width << '\n';
    outfile << "height = " << tilemap_height << '\n';
    outfile << "data = [" << data << "]\n\n";

    outfile << "[physic]" << '\n';

    auto body = physic->FirstChildElement("object");

    while (body != nullptr) {
        int x = 0, y = 0, id = 0;

        body->QueryAttribute("x", &x);
        body->QueryAttribute("y", &y);
        body->QueryAttribute("id", &id);

        auto data = body->FirstChildElement("polygon");

        if (data == nullptr) {
            data = body->FirstChildElement("polyline");
        }

        if (data == nullptr) {
            std::cerr << "Couldn't access polygon data on object " << id << '\n';
            body = body->NextSiblingElement("object");
            continue;
        }

        const char* points = "";

        data->QueryStringAttribute("points", &points);

        std::string points_str(points);

        outfile << "  [[physic.body]]" << '\n';
        outfile << "  position = " << '[' << x << ',' << y << "]\n";
        outfile << "  polygon = " << '[';

        auto ws_pos = points_str.find_first_of(' ');
        auto last_ws_pos = 0;

        while (ws_pos != std::string::npos) {
            if (last_ws_pos > 0) {
                ++last_ws_pos;
            }
            outfile << '[' << points_str.substr(last_ws_pos, ws_pos - last_ws_pos) << "],";
            last_ws_pos = ws_pos;
            ws_pos = points_str.find(' ', last_ws_pos + 1);
        }
        outfile << '[' << points_str.substr(last_ws_pos + 1, points_str.length() - last_ws_pos) << "]";

        outfile << "]\n";

        body = body->NextSiblingElement("object");
    }
    outfile << '\n';

    outfile << "[entities]" << '\n';
}

int main(int argc, char** args) {

    if (argc < 2) {
        std::cerr << "Expects at least one argument" << std::endl;
        return 1;
    }

    for (int i = 1; i < argc; ++i) {
        process_file(args[i]);
    }

    return 0;
}
