/*************************************************************************/
/*  main.cpp                                                             */
/*************************************************************************/
/*                       This file is part of:                           */
/*                            Polygonauts                                */
/*                            sinz.com.br                                */
/*************************************************************************/
/* Copyright (c) 2018-2020 Sinz Group.                                   */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*                                                                       */
/*************************************************************************/
#include <sun.hpp>
#include <core/filesys/toml.hpp>
#include <scene.hpp>
#include <resources.hpp>
#include <graphics.hpp>
#include <physics.hpp>
#include <script.hpp>

class Polygonauts : public sun::Application
{
public:

    enum class GameState {
        SplashScreen,
        Menu,
        Level
    };

    Polygonauts(sun::Context& context)
    :   sun::Application(context),
        scene_(context),
        polymorph_intensity_(0.f),
        splash_timer_(0.f),
        state_(GameState::SplashScreen)
    {
        window_.set_title("Polygonauts");
        //window_.set_size(1920, 1080);
        //window_.set_fullscreen(true);
        //window_.set_visible_cursor(false);
        window_.set_vsync(true);

        auto renderer = context_.get_system<sun::Renderer>();

        renderer->set_viewport({0, 0, (float)window_.get_size().x, (float)window_.get_size().y});
        renderer->set_projection(sun::Matrix4::orthogonal(0, (float)window_.get_size().x, (float)window_.get_size().y, 0));

        polymorph_shader_ = renderer->create_shader("res/polymorph.glsl");

        auto cache = context_.get_system<sun::ResourceCache>();
        cache->set_path("res/");

        auto logo_texture = cache->get_resource<sun::Texture>("sinz_logo.png");

        auto logo = scene_.create_entity();
        auto logo_sprite = logo->create_component<sun::Sprite>();

        logo_sprite->set_texture(logo_texture.get());
        logo_sprite->set_color({0, 0, 0, 0});

        logo->set_position(sun::Vector2i::to_vector2f(window_.get_size()) / 2);
        logo->set_origin(sun::Vector2i::to_vector2f(logo_texture->get_size()) / 2);

        auto fade_anim = logo->create_component<sun::Animation>();

        auto& track = fade_anim->create_track(*logo_sprite, "color", sun::Time::seconds(6.f));
        track.insert_key({sun::Color(0, 0, 0, 0), sun::Time::seconds(0.f)});
        track.insert_key({sun::Color::WHITE, sun::Time::seconds(2.f)});
        track.insert_key({sun::Color::WHITE, sun::Time::seconds(4.f)});
        track.insert_key({sun::Color(0, 0, 0, 0), sun::Time::seconds(6.f)});

        scene_.init();
    }

    void build_main_menu()
    {
        scene_.clear();

        auto renderer = context_.get_system<sun::Renderer>();
        auto cache = context_.get_system<sun::ResourceCache>();

        // Set camera to Identity Matrix
        renderer->set_camera_transform(sun::Matrix4());

        auto title = scene_.create_entity();
        auto title_text = title->create_component<sun::Text>();

        title_text->set_font(cache->get_resource<sun::Font>("mono.ttf").get());
        title_text->set_text("Polygonauts v0.2.3\nDev Menu\n"
        "Press number to choose scene:\n\n"
        "1- Testmap\n"
        "2- Ruins");

        state_ = GameState::Menu;

        scene_.init();
    }

    void build_scene(const std::string& map_name)
    {
        scene_.clear();

        auto t = sun::toml::parse("res/" + map_name);

        auto cache = context_.get_system<sun::ResourceCache>();

        auto columns = *t->get_qualified_as<int>("tileset.columns");
        auto tile_width = *t->get_qualified_as<int>("tileset.width");
        auto tile_height = *t->get_qualified_as<int>("tileset.height");
        auto tex_file = t->get_qualified_as<std::string>("tileset.image");

        auto w = *t->get_qualified_as<int>("tilemap.background.width");
        auto h = *t->get_qualified_as<int>("tilemap.background.height");
        auto data = *t->get_qualified_array_of<sun::int64>("tilemap.background.data");

        auto bodies = t->get_table("physic")->get_table_array("body");

        auto tileset_texture = cache->get_resource<sun::Texture>(*tex_file);
        tileset_texture->set_address_mode(sun::Texture::AddressMode::Wrap);

        auto tilemap = scene_.create_entity();
        auto tilemap_bg = tilemap->create_component<sun::SpriteBatch>();

        tilemap_bg->set_texture(tileset_texture.get());

        for (int y = 0 ; y < h ; ++y) {
            for (int x = 0 ; x < w ; ++x) {
                int tile = data[x + (y * w)];
                if (tile != 0) {
                    tilemap_bg->add_sprite_rect({(float)x * 64, (float)y * 64},
                        {((tile - 1) % columns) * tile_width, ((tile - 1) / columns) * tile_width, tile_width, tile_height});
                }
            }
        }

        w = *t->get_qualified_as<int>("tilemap.main.width");
        h = *t->get_qualified_as<int>("tilemap.main.height");
        data = *t->get_qualified_array_of<sun::int64>("tilemap.main.data");

        auto tilemap_m = tilemap->create_component<sun::SpriteBatch>();

        tilemap_m->set_texture(tileset_texture.get());

        for (int y = 0 ; y < h ; ++y) {
            for (int x = 0 ; x < w ; ++x) {
                int tile = data[x + (y * w)];
                if (tile != 0) {
                    tilemap_m->add_sprite_rect({(float)x * 64, (float)y * 64},
                        {((tile - 1) % columns) * tile_width, ((tile - 1) / columns) * tile_width, tile_width, tile_height});
                }
            }
        }

        spawn_player({10.f, 10.f});

        w = *t->get_qualified_as<int>("tilemap.foreground.width");
        h = *t->get_qualified_as<int>("tilemap.foreground.height");
        data = *t->get_qualified_array_of<sun::int64>("tilemap.foreground.data");

        auto tilemap_fg = tilemap->create_component<sun::SpriteBatch>();

        tilemap_fg->set_texture(tileset_texture.get());

        for (int y = 0 ; y < h ; ++y) {
            for (int x = 0 ; x < w ; ++x) {
                int tile = data[x + (y * w)];
                if (tile != 0) {
                    tilemap_fg->add_sprite_rect({(float)x * 64, (float)y * 64},
                        {((tile - 1) % columns) * tile_width, ((tile - 1) / columns) * tile_width, tile_width, tile_height});
                }
            }
        }

        if (bodies)
        {
            for (const auto& body : *bodies) {
                auto tile_body_component = tilemap->create_component<sun::RigidBody>();
                auto pos = body->get_array_of<sun::int64>("position");

                auto polygon = body->get_array_of<cpptoml::array>("polygon");

                sun::shapes::Convex shape;
                shape.set_point_count(polygon->size());

                int i = 0;
                for (const auto& point : *polygon) {
                    auto arr = point->as_array()->get();
                    auto x = arr[0]->as<sun::int64>()->get();
                    auto y = arr[1]->as<sun::int64>()->get();

                    shape.set_point(i++, {static_cast<float>(x) +
                                            static_cast<float>((*pos)[0]),
                                          static_cast<float>(y) +
                                            static_cast<float>((*pos)[1])});
                }

                tile_body_component->create(shape, sun::RigidBody::Type::Static);
            }
        }

        auto renderer = context_.get_system<sun::Renderer>();

        renderer->set_color({0, 230, 130});

        state_ = GameState::Level;

        scene_.init();
    }

    void spawn_player(const sun::Vector2f& pos)
    {
        auto res_cache = context_.get_system<sun::ResourceCache>();
        auto tex = res_cache->get_resource<sun::Texture>("body_92.png");
        auto eye_tex = res_cache->get_resource<sun::Texture>("eye.png");

        tex->set_filter_mode(sun::Texture::FilterMode::Trilinear);
        eye_tex->set_filter_mode(sun::Texture::FilterMode::Trilinear);

        auto player = scene_.create_entity("Jack");
        player_ = player;

        auto feye = player->create_child("feye");
        auto beye = player->create_child("beye");

        auto beye_sprite = beye->create_component<sun::Sprite>();

        auto sprite = player->create_component<sun::Sprite>("sprite");
        auto body = player->create_component<sun::RigidBody>("body");
        auto raycast = player->create_component<sun::Raycast>("raycast");
        camera_ = player->create_component<sun::Camera>();
        auto script = player->create_component<sun::Script>();

        auto feye_sprite = feye->create_component<sun::Sprite>();

        script->load("res/player.lua");
        camera_->set_follow(true);
        camera_->set_follow_speed(10.f);

        body->create(sun::shapes::Convex({{-32, 18}, {0, -36}, {32, 18}}),
            sun::RigidBody::Type::Dynamic);

        body->set_density(1.0);
        body->set_friction(0.8);
        body->set_angular_damping(3);

        raycast->cast_to({0.f, 35.f});

        sprite->set_texture(tex.get());
        sprite->set_shader(polymorph_shader_);
        feye_sprite->set_texture(eye_tex.get());
        //feye_sprite->set_shader(polymorph_shader_);
        beye_sprite->set_texture(eye_tex.get());
        //beye_sprite->set_shader(polymorph_shader_);

        player->set_scale(0.75f, 0.75f);
        player->set_origin(46, 54);
        player->set_position(pos);

        feye->set_scale(0.6f, 0.6f);
        beye->set_scale(0.6f, 0.6f);
        feye->set_position(-10, -30);
        beye->set_position(0, -30);
        feye->set_rotation_bit(false);
        beye->set_rotation_bit(false);
    }

    void on_update(float delta) override {

        switch (state_)
        {
            case GameState::SplashScreen:
                splash_timer_ += delta;
                if (splash_timer_ > 6.f) {
                    build_main_menu();
                }
                break;
            case GameState::Menu:
                break;
            case GameState::Level:
                break;
        }

    }

    void on_event(sun::Event& event) override {
        sun::Application::on_event(event);

        if (state_ == GameState::Menu) {
            if (event.type == sun::EventType::KeyPressed) {
                switch (event.key.code)
                {
                    case sun::keyboard::Key::Num1:
                        build_scene("maps/TestMap");
                        break;
                    case sun::keyboard::Key::Num2:
                        build_scene("maps/Ruins");
                        break;
                    default:
                        break;
                }
            }
        }
        if (state_ == GameState::Level) {
            if (event.type == sun::EventType::KeyPressed) {
                if (event.key.code == sun::keyboard::Key::Escape) {
                    build_main_menu();
                }
                if (event.key.code == sun::keyboard::Key::Add) {
                    /*polymorph_intensity_ += 0.051f;
                    polymorph_shader_->send("intensity", polymorph_intensity_);
                    player_->scale(polymorph_intensity_ * 0.007f, polymorph_intensity_ * 0.007f);*/
                    camera_->zoom(0.001f);
                }
                if (event.key.code == sun::keyboard::Key::Subtract) {
                    /*polymorph_intensity_ -= 0.051f;
                    polymorph_shader_->send("intensity", polymorph_intensity_);
                    player_->scale(-polymorph_intensity_ * 0.007f, -polymorph_intensity_ * 0.007f);*/
                    camera_->zoom(-0.001f);
                }
            }
        }
    }
private:

    sun::SceneTree  scene_;
    sun::Shader*    polymorph_shader_;
    sun::Entity*    player_;
    sun::Ref<sun::Camera>    camera_;
    float           polymorph_intensity_;
    float           splash_timer_;
    GameState       state_;
};

SUN_DEFINE_MAIN_APP(Polygonauts)
