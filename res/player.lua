-- Utility enum function
function enum(names, offset)
	offset=offset or 1
	local objects = {}
	local size=0
	for idr,name in pairs(names) do
		local id = idr + offset - 1
		local obj = {
			id=id,       -- id
			idr=idr,     -- 1-based relative id, without offset being added
			name=name    -- name of the object
		}
		objects[name] = obj
		objects[id] = obj
		size=size+1
	end
	objects.idstart = offset        -- start of the id range being used
	objects.idend = offset+size-1   -- end of the id range being used
	objects.size=size
	objects.all = function()
		local list = {}
		for _,name in pairs(names) do
			add(list,objects[name])
		end
		local i=0
		return function() i=i+1 if i<=#list then return list[i] end end
	end
	return objects
end

-- Player Constants
local max_angular_vel       = 120
local max_linear_vel        = 260
local air_max_linear_vel    = 180
local walk_lin_impulse      = Vector2:new(10, 0)
local air_lin_impulse       = Vector2:new(5, 0)
local walk_impulse_point    = Vector2:new(0, -18)
local walk_ang_impulse      = 4
local air_ang_impulse       = 1
local jump_impulse          = 180

-- Child Entities and Components
local body = nil
local raycast = nil

local feye = nil
local beye = nil

-- Utility vars
local current_dir = 1

local moving_left = false
local moving_right = false
local breaking = false

local jumping = false
local enable_jump = false
local can_double_jump = false

-- State control
local states = enum({"on_ground", "on_air", "on_water"})
local current_state = states.on_ground
local last_state = states.on_ground

function init(entity)

    feye = entity:find_child("feye", false)
    beye = entity:find_child("beye", false)

    body = entity:get_rigid_body("body")
    raycast = entity:get_raycast("raycast")

    body.friction = 2.0
end

function update(entity, delta)

    last_state = current_state
    if not raycast:is_colliding() then
        current_state = states.on_air
    else
        current_state = states.on_ground
        if last_state == states.on_air then
            can_double_jump = false
        end
    end

    if current_state == states.on_ground then

        if moving_right then
            if body.linear_velocity.x < max_linear_vel then
                body:apply_linear_impulse(walk_lin_impulse, walk_impulse_point)
            end
            if body.angular_velocity > -max_angular_vel then
                body:apply_angular_impulse(-walk_ang_impulse)
            end
        end

        if moving_left then
            if body.linear_velocity.x > -max_linear_vel then
                body:apply_linear_impulse(Vector2:new(-10, 0), walk_impulse_point)
            end
            if body.angular_velocity < max_angular_vel then
                body:apply_angular_impulse(walk_ang_impulse)
            end
        end

        if jumping and enable_jump then
            body:apply_linear_impulse(Vector2:new(0, -jump_impulse))
            enable_jump = false
            can_double_jump = true
        end

        if breaking then
            max_linear_vel = 120
        else
            max_linear_vel = 240
        end

    elseif current_state == states.on_air then

        if moving_right then
            if body.linear_velocity.x < air_max_linear_vel then
                body:apply_linear_impulse(air_lin_impulse)
            end
            body:apply_angular_impulse(-air_ang_impulse)
        end

        if moving_left then
            if body.linear_velocity.x > -air_max_linear_vel then
                body:apply_linear_impulse(Vector2:new(-5, 0))
            end
            body:apply_angular_impulse(air_ang_impulse)
        end

        if jumping and enable_jump and can_double_jump then
            body:apply_linear_impulse(Vector2:new(0, -jump_impulse))
            enable_jump = false
            can_double_jump = false
        end
    end

end

function look_dir(dir)
    current_dir = dir
    if dir == 0 then
        feye.position.x = 10
        feye.scale.x = -0.6
        beye.scale.x = -0.6
    elseif dir == 1 then
        feye.position.x = -10
        feye.scale.x = 0.6
        beye.scale.x = 0.6
    end
end

function handle_events(entity, event)

    if event.type == EventType.KeyPressed then
        if event.key.code == Key.R then
            entity.position = Vector2:new(10, 0)
            if body ~= nil then body:move_to_entity() end
        end
        if event.key.code == Key.Left then
            if current_dir ~= 0 then
                look_dir(0)
            end
            moving_left = true
        end
        if event.key.code == Key.Right then
            if current_dir ~= 1 then
                look_dir(1)
            end
            moving_right = true
        end
        if event.key.code == Key.Down then
            breaking = true
        end

        if event.key.code == Key.Up and not jumping then
            jumping = true
            enable_jump = true
        end
    end

    if event.type == EventType.KeyReleased then
        if event.key.code == Key.Left then
            moving_left = false
        end
        if event.key.code == Key.Right then
            moving_right = false
        end
        if event.key.code == Key.Down then
            breaking = false
        end
        if event.key.code == Key.Up then
            jumping = false
        end
    end
end
