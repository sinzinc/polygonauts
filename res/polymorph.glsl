#VERTEX

#version 130
#extension GL_ARB_explicit_attrib_location : enable
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 att_pos;
layout(location = 1) in vec2 att_tex_uv;
layout(location = 2) in vec4 att_color;

out vec4 color;
out vec2 tex_uv;

uniform mat4 model = mat4(1.0);
uniform mat4 projection = mat4(1.0);
uniform mat4 viewport = mat4(1.0);

void main()
{
    gl_Position = projection * viewport * model * vec4(att_pos, 0.0, 1.0);
    color = att_color;
    tex_uv = att_tex_uv;
}

#FRAGMENT

#version 130

in vec4 color;
in vec2 tex_uv;

out vec4 frag_color;

uniform sampler2D tex;
uniform float intensity = 0.0;

void main()
{
    vec2 center = vec2(0.5 , 0.5);

    frag_color = (color * texture(tex, tex_uv)) +
        (vec4(1.0, 1.0, 1.0, 0.0) * intensity) / distance(center, tex_uv) * 2;

    if (frag_color.w > 0.0) {
        frag_color.w -= intensity * distance(center, tex_uv);
    }
}
